import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the TarefasServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TarefasServiceProvider {

  tarefas = [
    { codigo: 1, projeto: 1, descricao: 'Elaborar prova', data: new Date(2017, 9, 22), prioridade: 1 },
    { codigo: 2, projeto: 1, descricao: 'FecharDiario', data: new Date(2017, 10, 29), prioridade: 2 },
    { codigo: 3, projeto: 2, descricao: 'Elaborar video', data: new Date(2018, 0, 13), prioridade: 1 },
    { codigo: 4, projeto: 3, descricao: 'Campanha', data: new Date(2017, 9, 27), prioridade: 3 },

  ];
  ultimoCodigo = 4;

  constructor(public http: Http) {
    console.log('Hello TarefasServiceProvider Provider');
  }

  getTarefas(): any[] {
    return this.tarefas;
  }


  editarTarefa(c, prj, dsc, data, prioridade) {
    for (let i = 0; i < this.tarefas.length; i++) {
      if (this.tarefas[i].codigo == c) {
        this.tarefas[i].projeto = prj;
        this.tarefas[i].descricao = dsc;
        this.tarefas[i].data = data;
        this.tarefas[i].prioridade = prioridade;
        break;
      }
    }
  }

  excluir(c) {

    for (let i = 0; i < this.tarefas.length; i++) {
      if (this.tarefas[i].codigo == c) {
        this.tarefas.splice(i, 1);
        break;
      }
    }
  }

  addTarefa(prj, dsc, data, prioridade) {
    this.ultimoCodigo++;

    this.tarefas.push({
      codigo: this.ultimoCodigo,
      projeto: prj,
      descricao: dsc,
      data: data,
      prioridade: prioridade
    })
  }

}
