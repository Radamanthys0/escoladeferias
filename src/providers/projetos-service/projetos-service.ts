import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ProjetosServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProjetosServiceProvider {

  projetos = [
    { codigo: 1, nome: 'AED' },
    { codigo: 2, nome: 'EAD' },
    { codigo: 3, nome: 'bla' }
  ];

  ultimoCódigo = 3;

  constructor(public http: Http) {
    console.log('Hello ProjetosServiceProvider Provider');
  }

  getprojetos() {
    return this.projetos;
  }

  editProjeto(c: number, n: string) {
    for (let i = 0; i < this.projetos.length; i++) {
      if (this.projetos[i].codigo == c) {
        this.projetos[i].nome = n;
        break;
      }
    }
  }

  deleteProjeto(c: number) {
    for (let i = 0; i < this.projetos.length; i++) {
      if (this.projetos[i].codigo == c) {
        this.projetos.splice(i, 1);
        break;
      }
    }
  }

  addProjeto(n: string) {
    this.ultimoCódigo++;
    this.projetos.push({
      codigo: this.ultimoCódigo,
      nome: n
    })
  }

}
