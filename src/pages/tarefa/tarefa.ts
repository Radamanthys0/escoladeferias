import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ProjetosServiceProvider } from './../../providers/projetos-service/projetos-service';
import { TarefasServiceProvider } from './../../providers/tarefas-service/tarefas-service';

/**
 * Generated class for the TarefaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tarefa',
  templateUrl: 'tarefa.html',
})
export class TarefaPage {

  rootPage = null;

  projetos: any[];
  novo: boolean;

  codTarefa: number;
  codProjeto: number;
  descricao: string;
  prioridade: number;
  data: string;



  constructor(public navCtrl: NavController, public navParams: NavParams,
    public tarefasService: TarefasServiceProvider,
    public projetoService: ProjetosServiceProvider) {

    this.novo = navParams.get('novo');
    this.codTarefa = navParams.get('codigo');
    this.projetos = projetoService.getprojetos();

    if (!this.novo) {
      let tarefas = tarefasService.getTarefas();
      for (let i = 0; i < tarefas.length; i++) {
        if (tarefas[i].codigo == this.codTarefa) {
          this.codProjeto = tarefas[i].projeto;
          this.descricao = tarefas[i].descricao;
          this.prioridade = tarefas[i].prioridade;
          let d = tarefas[i].data;

          this.data = d.getFullYear() + "-" +
            ("0" + (d.getMonth() + 1)).substr(-2, 2) + "-" +
            ("0" + d.getDate()).substr(-2.2);

        }
      }
    } else {
      this.codProjeto = this.projetos[0].codigo
      this.descricao = '';
      this.prioridade = 3
      let d = new Date();

      this.data = d.getFullYear() + "-" +
        ("0" + (d.getMonth() + 1)).substr(-2, 2) + "-" +
        ("0" + d.getDate()).substr(-2.2);
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TarefaPage');
  }


  alterar() {
    let d = new Date(parseInt(this.data.substr(0, 4)), parseInt(this.data.substr(5, 2))-1, parseInt(this.data.substr(8, 2)));
    console.log(d);
    this.tarefasService.editarTarefa(this.codTarefa,
      this.codProjeto,
      this.descricao,
      d,
      this.prioridade);
    this.navCtrl.pop();
  }
  excluir() {
    console.log(this.codTarefa);
    this.tarefasService.excluir(this.codTarefa);
    this.navCtrl.pop();
  }

  incluir() {
    let d = new Date(parseInt(this.data.substr(0, 4)), parseInt(this.data.substr(5, 2))-1, parseInt(this.data.substr(8, 2)));

    this.tarefasService.addTarefa(
      this.codProjeto,
      this.descricao,
      d,
      this.prioridade);
    this.navCtrl.pop();

  }

}