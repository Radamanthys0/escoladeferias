import { TarefasPage } from './../tarefas/tarefas';
import { ProjetosPage } from './../projetos/projetos';
import { Component } from '@angular/core';

// import { AboutPage } from '../about/about';
// import { ContactPage } from '../contact/contact';
// import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = TarefasPage;  
  tab2Root = ProjetosPage;
  // tab2Root = AboutPage;
  // tab3Root = ContactPage;

  constructor() {

  }
}
