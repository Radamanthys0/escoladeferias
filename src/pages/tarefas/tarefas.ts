import { TarefaPage } from './../tarefa/tarefa';
import { ProjetosServiceProvider } from './../../providers/projetos-service/projetos-service';
import { TarefasServiceProvider } from './../../providers/tarefas-service/tarefas-service';
import { Component, Pipe, PipeTransform } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

/**
 * Generated class for the TarefasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tarefas',
  templateUrl: 'tarefas.html',
})
export class TarefasPage {

  tarefas: any[];
  projetos: any[];
  filtroTarefas = {};


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public tarefasService: TarefasServiceProvider,
    public projetoService: ProjetosServiceProvider,
    public menuctrl: MenuController, ) {

    this.tarefas = tarefasService.getTarefas();
    this.projetos = projetoService.getprojetos();
  }

  ionViewDidLoad() {
  }

  nomeProjeto(c) {
    for (let i = 0; i < this.projetos.length; i++) {
      if (this.projetos[i].codigo == c) {
        return this.projetos[i].nome;
      }
    }
    return "Projeto Não encontrado";
  }
  selecionaTarefa(c) {
    let t: number = parseInt(c);
    this.navCtrl.push(TarefaPage, { codigo: t, novo: false });
  }

  novaTarefa() {
    this.navCtrl.push(TarefaPage, { codigo: 0, novo: true });
  }
  limparFiltros() {
    this.filtroTarefas = {};
    this.menuctrl.close();
  }

  filtroProjeto(c) {
    this.filtroTarefas = { projeto: c };
    this.menuctrl.close();
  }
  filtroDias(d) {
    this.filtroTarefas = { dias: d };
    this.menuctrl.close;
  }
}


@Pipe({
  name: 'filtro'
})
export class Filtro implements PipeTransform {
  transform(itens: any[], filtro: any): any {

    itens.sort((a, b) => a.data - b.data);

    console.log(filtro.dias);
    if (filtro.projeto >= 0) {
      let g = itens.filter(item => item.projeto == filtro.projeto);
      return g;
    } else if (filtro.dias >= 0) {
      //algo

      let d = new Date((new Date()).getTime() + filtro.dias * 24 * 60 * 60 * 1000);
      // d.setHours(23,59,59,999);
      return itens.filter(item => item.data <= d);
    } else {
      return itens;
    }
  }
}

