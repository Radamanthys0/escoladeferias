import { ProjetoPage } from './../projeto/projeto';
import { ProjetosServiceProvider } from './../../providers/projetos-service/projetos-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the ProjetosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-projetos',
  templateUrl: 'projetos.html',
})
export class ProjetosPage {

  projetos : any[];
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public projetoService:ProjetosServiceProvider) {

                this.projetos = projetoService.getprojetos();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjetosPage');
  }

  SelecionaProjeto(c){
    let cn = parseInt(c);
    this.navCtrl.push(ProjetoPage, {codigo: cn, novo:false});
    console.log(cn);
  }

  novoProjeto(){
    this.navCtrl.push(ProjetoPage, {codigo: 0, novo:true});
  
  }


}
